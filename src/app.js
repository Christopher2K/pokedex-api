if (process.env.NODE_ENV !== 'production') require('dotenv').config()
const express = require('express')
const http = require('http')
const socketio = require('socket.io')
const bodyParser = require('body-parser')
const morgan = require('morgan')

const routes = require('./routes')

const app = express()
const port = process.env.PORT || 3001

const server = http.createServer(app)
const io = socketio(server)

app.use(bodyParser.json())
app.use(morgan('tiny'))

routes(app, io)

server.listen(port, () => {
  console.log(`server is up at localhost:${port}`)
})
