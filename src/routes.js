const Twitter = require('twitter')

module.exports = (app, io) => {
  let twitter = new Twitter({
    consumer_key: 'KEY',
    consumer_secret: 'SECRET',
    access_token_key: 'LOL',
    access_token_secret: 'NOPE'
  })

  app.get('/', (_, res) => res.send(`Err'thang is goooood`))

  let streaming

  function stream ({ term, socket }) {
    streaming = twitter.stream('statuses/filter', { track: term })
    streaming.on('data', (tweet) => {
      if (!tweet.text.includes('RT')) {
        socket.emit('tweets', tweet)
      }
    })

    streaming.on('error', (error) => {
      console.log(error)
      socket.emit('tweet_error', { message: 'DEAD_STREAMING' })
    })
  }

  io.on('connection', async (socket) => {
    const term = socket.handshake.query['term'] || 'pokemon'
    console.log(`Streaming Twitter API for ${term}`)
    if (streaming) {
      console.log(`New connection, destroying previous stream`)
      await streaming.destroy()
    }

    let statuses = [{ statuses: [] }]

    try {
      statuses = await twitter.get('search/tweets', {
        q: term,
        result_type: 'recent'
      })
    } catch (e) {
      socket.emit('tweet_error', { message: 'DEAD_DATA' })
    }

    socket.emit('initial', statuses)
    stream({ term, socket })
    socket.on('disconnect', () => {
      console.log('Client disconnected')
      streaming.destroy()
    })
  })
}
